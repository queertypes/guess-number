{-# LANGUAGE OverloadedStrings #-}
module Game.Guess.Number.Core (
  run
) where

import Safe (readMay)
import System.Random

loop :: Int -> IO ()
loop n = do
  putStr "Guess a number between 0-10> "
  x <- getLine
  case (readMay x :: Maybe Int) of
    (Just userGuess) -> if userGuess == n
                        then putStrLn "You win!"
                        else putStrLn "Try again" >> loop n
    Nothing          -> putStrLn (x ++ " is not a number") >> loop n

run :: IO ()
run = do
  numberToGuess <- randomRIO (0,10)
  loop numberToGuess
